package com.cpt202.rent.models;

public class User {
    String name;
    String drivingLicense;
    String phoneNumber;
    
    public User() {
    }
    
    
    public User(String name, String drivingLicense, String phoneNumber) {
        this.name = name;
        this.drivingLicense = drivingLicense;
        this.phoneNumber = phoneNumber;
    }
   
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDrivingLicense() {
        return drivingLicense;
    }
    public void setDrivingLicense(String drivingLicense) {
        this.drivingLicense = drivingLicense;
    }
    public String getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
